

「[连享会2019计量方法与经典论文专题班]([https://gitee.com/arlionn/Course/blob/master/2019Papers.md](https://gitee.com/arlionn/Course/blob/master/2019Papers.md))」（[微信版]([https://mp.weixin.qq.com/s/1zfVO2GvCaYSwARmugsLLQ](https://mp.weixin.qq.com/s/1zfVO2GvCaYSwARmugsLLQ))）即将开始。为保证本次课程的全方位答疑解惑，现招聘培训课程助手数名，主要辅助老师的教学和答疑工作。助教可以免费参加此次培训课程。

>#### 具体说明和要求：

- **名额：** 6 名

- **任务：** (1) 做好开课前的准备工作，在老师的指导下撰写 3-5 篇介绍 Stata 和计量经济学基础知识的文档；(2) 协助老师的教学工作；(3) 负责课间和课后答疑，以及课程总结推文编辑工作 (Note: 下午 5:00-5:30 的现场答疑由主讲教师负责)。

- **要求：** 热心、尽职，熟悉 Stata 的基本语法和常用命令，能对常见问题进行解答和记录。

- **申请截止时间：** 2019 年 7 月 23 日 (将于 2019.7.25 日公布遴选结果)

>#### 申请方式：在线提交表单

助教申请网址：[https://www.wjx.top/jq/42189355.aspx](https://www.wjx.top/jq/42189355.aspx)

亦可扫码填写助教申请资料：

![连享会-2019计量方法与经典论文专题班助教招聘-扫码填写资料](https://images.gitee.com/uploads/images/2019/0728/214827_f461077f_1522177.png "连享会-2019暑期Stata现场班助教招聘-扫码填写资料")


&emsp;

&emsp;


[![2019暑期“实证研究方法与经典论文”专题班-连玉君-江艇主讲](https://images.gitee.com/uploads/images/2019/0728/214827_9db20cec_1522177.png)](https://gitee.com/arlionn/Course/blob/master/2019Papers.md)



### 附：连享会2019「计量方法与经典论文专题班」课程简介

---

#### A. 课程概要

- **课程主页：** https://gitee.com/arlionn/Course/edit/master/2019Papers.md
- **时间：** 2019 年 8 月 17-23 日    
- **地点：** 西安，西北工业大学国际会议中心  ([百度地图](https://j.map.baidu.com/yXIiP) | [搜狗地图](http://map.sogou.com/u/MvmiOn))
- **主讲嘉宾**：
  - 连玉君 (中山大学，8 月 17-19 日，专题 T1-T6)
  - 江艇 (中国人民大学，8 月 21-23 日，专题 T7-T12)
- **授课内容：** 实证研究方法与经典论文 (9 篇) 重现
  - **课程要点：** 研究设计、稳健性检验、内生性问题及论文的写作。
  - **主要方法：** IV 估计、匹配 (Matching)、倍分法 (DID)、Matching+DID、双向固定效应模型、动态面板数据模型等

##### 课程特色

1. **渔非鱼。** 体系完整，注重模型设定思路、研究设计和结果的解释。第 1-3 讲介绍研究设计和主要回归方法；第 4-12 讲深度解构 9 篇发表于 Top 期刊的经典论文的实现过程。[[课程中涉及的论文一览]](https://gitee.com/arlionn/stata_training/tree/master/PDF)
2. **电子板书。** 全程电子板书，听课更专注，复习更高效；【**下载**】[连玉君-FE-板书](https://gitee.com/arlionn/stata_training/raw/master/Stata%E6%9D%BF%E4%B9%A6/Stata%E6%9D%BF%E4%B9%A6-FE-version2.pdf)
3. **可重现。** 我们会提供重现论文所需的全套课件，包括 Do-files、范例数据、自编程序，课程中的估计方法和代码都可以快速移植到自己的论文中。【**下载**】[A1_intro.do](https://gitee.com/arlionn/stata_training/raw/master/dofile-data/A1_intro.pdf),  [A6_DID.do](https://gitee.com/arlionn/stata_training/raw/master/dofile-data/A6_DID.pdf)
4. **注重方法的合理搭配。**  基于每篇文章的重现，重点讲解如下几个论文写作中普遍面临但又非常棘手的问题：
   - 研究设计如何开展？
   - 稳健性检验包括哪些内容？主要有哪些方法和途径？
   - 内生性问题如何确认和解决？
   - 如何解读和表述实证结果？


#### B. 讲授嘉宾简介

---

![连玉君](https://images.gitee.com/uploads/images/2019/0728/214827_e73c470f_1522177.jpeg)   
**[连玉君](http://lingnan.sysu.edu.cn/node/151)** ，经济学博士，副教授，博士生导师。2007年7月毕业于西安交通大学金禾经济研究中心，现任教于中山大学岭南学院金融系。主讲课程为“金融计量”、“计量分析与Stata应用”、“实证金融”等。已在《China Economic Review》、《经济研究》、《管理世界》、《经济学(季刊)》、《金融研究》、《统计研究》等期刊发表论文60余篇。连玉君副教授主持国家自然科学基金项目（2项）、教育部人文社科基金项目、广东自然科学基金项目等课题项目10余项。目前已完成 Panel VAR、Panel Threshold、Two-tier Stochastic Frontier 等计量模型的 Stata 实现程序，并编写过几十个小程序，如 `xtbalance`, `winsor2`, `bdiff`, `hausmanxt`, `ttable3`, `hhi5`, `ua`等。连玉君老师团队一直积极分享Stata应用中的点点滴滴，开设了 [[Stata连享会-简书]](https://www.jianshu.com/u/69a30474ef33)，[[Stata连享会-知乎]](https://www.zhihu.com/people/arlionn) 两个专栏，并定期在微信公众号 (**StataChina**) 中发布精彩推文。

---

![江艇](https://images.gitee.com/uploads/images/2019/0728/214827_c343d911_1522177.png)   
**江艇**，香港科技大学商学院经济学博士，中国人民大学经济学院副教授，人大国家发展与战略研究院研究员，人大微观数据与实证方法研究中心副主任，美国哥伦比亚大学商学院访问学者。主要研究领域为经济增长与发展、城市经济学、新政治经济学，在*Economics Letters*、*Review of Development Economics*、《经济研究》、《管理世界》、《世界经济》等国内外著名学术刊物上发表多篇论文，曾应邀在多所高校讲授“应用微观计量经济学”短期前沿课程并广受好评。

&emsp;

### C. 课程主题

##### T1：研究设计基础：模型设定和解释

##### T2：分析工具：面板数据模型

##### T3：统计推断方法：自抽样 (Bootstrap) 和蒙特卡洛模拟分析 (Monte Carlo Simulation)

##### T4：如何讲故事？人口老龄化、机器人与经济增长 | Acemoglu and Restrepo (2017)
-  Acemoglu, D., P. Restrepo, **2017**, Secular stagnation? The effect of aging on economic growth in the age of automation, ***American Economic Review***, 107 (5): 174-179. [[PDF](https://gitee.com/arlionn/stata_training/raw/master/PDF/Acemoglu_2017_AER.pdf)]

##### T5：稳健性检验如何做？ Flannery and Rangan(2006)
- Flannery, M. J., K. P. Rangan, **2006**, Partial adjustment toward target capital structures, ***Journal of Financial Economics***, 79 (3): 469-506. [[PDF]](https://gitee.com/arlionn/stata_training/raw/master/PDF/Flannery_2006.pdf)


##### T6：计数模型的应用：论文引用率如何提高？
- Di Vaio, G., D. Waldenstrom J. Weisdorf, Citation Success: Evidence from Economic Histopry Journals, Explorations in Economic History, 49 (1): 92-104. [[PDF]](https://gitee.com/arlionn/stata_training/raw/master/PDF/Di_Vaio_2012.pdf), [[PPT]](https://gitee.com/arlionn/stata_training/blob/master/PDF/Di_Vaio_2012.ppt)

##### T7: 交互项模型中的工具变量方法
- Rajan, Raghura- G, and Luigi Zingales, 1998, “Financial Dependence and Growth.” *American Economic Review* 88(3): 559-86\. [[PDF](https://gitee.com/arlionn/stata_training/raw/master/PDF/Rajan_Zingales_1998_AER.pdf)]

##### T8: 工具变量法的必要性与效果
- Nunn, Nathan, and Leonard Wantchekon, 2011, “The Slave Trade and the Origins of Mistrust in Africa.” *American Economic Review*101(7): 3221-52\. [[PDF](https://gitee.com/arlionn/stata_training/raw/master/PDF/Nunn_Wantchekon_2011_AER.pdf)]

##### T9: 匹配方法原理
- Imbens, Guido W, 2015, “[Matching Methods in Practice](https://gitee.com/arlionn/stata_training/raw/master/PDF/Imbens_2015_JHR.pdf): Three Examples.” *Journal of Human Resources* 50(2): 373-419.

##### T10: 截面数据中匹配方法与工具变量法的综合运用
- Aidt, Toke S, and Raphael Franck, 2015, “Democratization Under the Threat of Revolution: Evidence Fro- the Great Refor- Act of 1832.” *Econometrica* 83(2): 505-47\. [[PDF](https://gitee.com/arlionn/stata_training/raw/master/PDF/Aidt_-2015-Econometrica.pdf)]

##### T11: 连续型处理与多期双重差分方法
- Nunn, N., and N. Qian, 2011, “The Potato's Contribution to Population and Urbanization: Evidence Fro- a Historical Experiment.” *Quarterly Journal of Economics* 126(2): 593–650\. [[PDF](https://gitee.com/arlionn/stata_training/raw/master/PDF/Nunn_2011_QJE.pdf)]

##### T12: 双重差分方法与匹配方法的结合
- Fowlie, et al., 2012, “What Do Emissions Markets Deliver and to Whom? Evidence Fro- Southern California's NOx Trading Program.” *American Economic Review* 102(2): 965-93\. [[PDF](https://gitee.com/arlionn/stata_training/raw/master/PDF/Fowlie_2012_AER.pdf)]


&emsp;

### 报名信息
>报名链接： https://www.wjx.top/jq/41753249.aspx     
或 长按/扫描二维码报名：   
![连享会-2019暑期学术论文专题](https://images.gitee.com/uploads/images/2019/0728/214827_a18f7e76_1522177.png)
>
>**温馨提示：** 按报名顺序挑选\安排座位
&emsp;

