
&emsp;

> #### [连享会 - 效率分析专题](https://www.lianxh.cn/news/a94e5f6b8df01.html)，2020年5月29-31日   
> 主讲嘉宾：连玉君 | 鲁晓东 | 张宁      
> &#x1F34F;   [课程主页](https://www.lianxh.cn/news/88426b2faeea8.html)，[微信版](https://mp.weixin.qq.com/s/zRotMGebIQqaMih8B71fdg)，[码云版](https://gitee.com/arlionn/TE)，[PDF版](https://quqi.gblhgk.com/s/880197/g3ne5HdXdrSoo8iL)

![连享会-效率分析专题视频](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/TE专题_连享会_lianxh.cn500.png "连享会-效率分析专题视频，三天课程，随时学")






## 课程概览

- **简介：** 本课程通过三个专题的讲解，系统介绍各类效率分析工具的原理和 Stata 实现方法，包括： **TFP  专题** (全要素生产率)、**DEA 专题** (数据包络分析)、**SFA 专题** (随机边界分析)。每个主题提供 1-2 篇论文的讲解，附带完整的重现文件，以便大家能应用于自己的实证分析之中。[课程大纲 PDF 版本](https://quqi.gblhgk.com/s/880197/g3ne5HdXdrSoo8iL)
- **课程主页：** <https://gitee.com/arlionn/TE> 
- **授课方式：网络直播** (15 天回放)，缴费后主办方发送邀请码点击邀请码即可进入在线课堂，收看直播无需安装任何软件。
- 时间：2020 年 5 月 29-31 日 (周五-周日)
  - **时段：** 上午 9:00-12:00; 下午 2:30-5:30；课后 30-60 分钟答疑
- **授课嘉宾：**
  - 鲁晓东 (中山大学)
  - 张 &ensp; 宁 (暨南大学)
  - 连玉君 (中山大学)
- **软件/课件：** Stata 软件，提供全套 Stata 实操程序、数据和 dofiles (开课前一周发送)
- **报名链接：** <http://junquan18903405450.mikecrm.com/FhWLhS2>

&emsp;

## 诚聘助教

[诚聘课程助教 (6名)](https://www.wjx.top/jq/73021543.aspx)，截止时间：2020 年 5 月 1 日
        

> **扫码报名：** https://www.wjx.top/jq/73021543.aspx  

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/二维码_TE_助教招聘150.png "连享会-效率分析专题-助教招聘")

&emsp;

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/横条-三人.jpg)
