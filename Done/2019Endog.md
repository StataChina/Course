# 连享会：内生性专题研讨班课程大纲


[![连享会-内生性专题班-报名页-2019.11.14-17](https://images.gitee.com/uploads/images/2019/0908/155222_62e4c77b_1522177.png)](http://junquan18903405450.mikecrm.com/ipdvr0X)


[toc]

---
## 1. 课程概览
- **时间：** 2019 年 11 月 14-17 日 (周四-周日)
- **时段：** 上午 9:00-12:00；下午 14:00-17:00，答疑：17:00-17:30
- **地点：** 南京财经大学福州路校区培训中心 (鼓楼区铁路北街128号) ([百度地图](https://j.map.baidu.com/29/EaK)) 
- **主讲嘉宾：** 林建浩 (中山大学)；王群勇 (南开大学)
- **课程主题：** [内生性问题及估计方法](http://junquan18903405450.mikecrm.com/ipdvr0X)
- **课程要点：** 整体上遵循 **「从文献中来，到论文中去」** 的原则。结合 Top 期刊论文 (AER, JPE, QJE, JoE, JF 等) 讲解目前主流的内生性问题应对方法和模型。在理解理论的基础上，通过 Stata 实操和论文解读来加强对模型的理解和应用。
- **主要方法：** 
  - 传统 **IV** 估计
  - 弱 IV 和近似外生 IV
  - **面板**数据模型：固定效应模型, 动态面板模型
  - 倍分法 (**DID**)：DID, 动态 DID 及三重差分 (DDD)
  - 断点回归 (**RDD**)
  - 倾向得分匹配分析 (**PSM**)
  - **处理效应**模型 (Treatment Effect model)
  - 控制函数法 (Control Function)
  - 内生 Probit/Logit 等
- **软件和课件：** 本次培训使用 Stata 软件。课程中涉及的所有数据、程序和论文实现代码，会在开课前一周发送给学员。

&emsp;

---
## 2. 嘉宾简介

### 王群勇
![王群勇](https://images.gitee.com/uploads/images/2019/0905/155357_5510cc8d_1522177.png)    
王群勇，经济学博士，南开大学数量经济研究所教授、博士生导师，在《Stata Journal》、《China Economic Review》、《Journal of Family and Economics Issue》等 SSCI 期刊发表多篇论文, 主持国家自然科学基金、教育部人文社科项目、天津市科技支撑项目等多项国家级和省部级课题。

### 林建浩
![林建浩](https://images.gitee.com/uploads/images/2019/0905/155357_ef72bb72_1522177.jpeg)   
**[林建浩](http://lingnan.sysu.edu.cn/node/160)** ，中山大学岭南学院经济学系副教授，博士生导师，主要研究领域为计量经济学理论与应用、实证宏观、文化与经济行为。在《经济研究》、《管理世界》、《统计研究》、《金融研究》、《经济学（季刊）》等学术刊物上发表多篇论文，主持国际自然科学基金、广东省自然科学基金等多项课题。
&emsp;


&emsp;

---
## 3. 课程内容及大纲

**「内生性」** 是实证分析过程中非常棘手的问题。本次专题课程对文献中使用的主要模型和方法做一个系统性的介绍。本次课程的两位授课嘉宾在解决内生性方面有深厚的理论功底和丰富的实战经验。因此，在课程中，一方面会尽力讲清楚模型背后的经济含义，另一方面则会以 Top 期刊论文为例来展示这些方法的实际应用，从而达到「学以致用」的目的。

**专题 1-2** 以 IV 估计为主题，依次介绍传统 IV 估计的基本思想和新近发展的「弱 IV」及「近似外生 IV」架构下的估计和统计推断方法。

工具变量 (IV) 估计是处理内生性问题的基本方法，在经济学实证研究中有着极为广泛的应用。
IV 估计量的大样本性质是建立在一系列严格的假设之上，最核心的有两个：一是工具变量与内生解释变量的「**相关性**」； 二是工具变量的「**外生性**」。显然，工具变量的「质量」会在很大程度上影响了 IV 估计量在有限样本下的表现。然而，在实际研究中，寻找同时满足相关性和外生性的工具变量非常困难，需要在经济机理上进行具有说服力的论证，同时进行严格的模型设定检验。越来越多的计量经济学家开始放松传统 IV 估计的模型设定，讨论「弱工具变量」(Weak Instruments) 以及 「近似外生工具变量」(plausibly /approximately/ exogenous) 下的稳健推断方法。    

针对工具变量外生性条件无法严格满足的情形，目前较有影响的做法是采用再抽样（resampling，Berkowitz et al., 2012）或贝叶斯的方法校正近似外生性的影响（Conley et al., 2012；Kraay, 2012）。     

这些新的理论研究成果直面实践难题，大大拓展了 IV 估计的应用场景，但尚未在实证研究中得到广泛应用。我们将结合实际例子，对这些方法的原理、适用条件以及软件实现进行介绍。

**专题 3** 介绍静态和动态面板数据模型。
由于面板资料的获取越来越方便，目前多数研究中使用的都是面板数据。使用面板的一个主要优势在于可以控制不可观测的个体效应，这有助于解决遗漏变量导致的内生性偏误。另一个优势是便于分析动态变化关系，这在动态面板数据模型中体现的最为清晰。棘手的是，动态面板模型天生就是一个包含内生性的模型，需要使用 GMM 进行估计，并执行一系列检验来确定工具变量的合理性。

在讲解这些模型的基本思想和估计方法的过程中，我们会将重点放在模型含义和应用范围上来。例如，对于同一笔数据而言，何时采用 OLS 进行估计，何时采用 FE 估计？动态面板中如何克服弱工具变量问题？不同的方法之间有何差异和关联？结果背后的经济含义如何解读？掌握这些方法有助于大家合理控制内生性问题，以便得到更为可信的结论。

**专题 4-6** 涵盖了目前在政策评价方面应用日益广泛的三种方法：倍分法 (**DID**)，倾向得分匹配分析 (**PSM**) 和断点回归 (**RDD**)。这些方法并没有用到太复杂的技术，而主要是在「**反事实**」架构下，通过合理的研究设计来估计政策效应。从估计方法的角度来看，DID 和 RDD 其实就是在普通的线性回归模型中增加虚拟变量和交乘项来捕捉政策带来的冲击；而 PSM 则是通过匹配的方式滤除样本选择或自选择带来的偏差，进而对匹配后的样本执行传统的假设检验或回归分析。

**专题 7** 介绍离散选择模型中的内生性问题及其处理方法。目前，微观数据的使用越发广泛，也使得文献中更多地应用 Logit, Probit 族模型。然而，对于此类模型中存在的内生性问题，目前的关注度却非常有限。本讲中介绍的方法能有效应对此类问题，以便得到更为可靠地统计推断结果。

**专题 8**  介绍用以解决自我选择偏误导致的内生性问题的模型。一是处理效应模型，主要应对解释变量中所包含的 0/1 内生变量 (主要源于自选择和样本选择偏误)。当被解释变量也为离散变量的情形，还需要一些特殊的处理方法，这是专题 8 讲解的重点。翻阅最近2年发表于《经济研究》、《管理世界》、《经济学(季刊)》等期刊的文章，这类模型在处理内生性问题方面得到了日益广泛的应用。

&emsp;

---
### 林建浩 (1-4 讲)


#### 专题 1：传统 IV 
- IV 估计与相应的模型设定检验
- 经典文献选取 IV 的基本思路
- 参考文献：
   - Levitt, Steven D. 1996. The Effect of Prison Population Size on Crime Rates: Evidence from Prison Overcrowding Litigation. **Quarterly Journal of Economics**. 111:2, pp. 319–51. [[PDF]](http://piketty.pse.ens.fr/files/Levitt1996.pdf)
   - Levitt, Steven D. 1997. Using Electoral Cycles in Police Hiring to Estimate the Effect of Police on Crime. **American Economic Review**. 87:4, pp. 270–90. [[PDF1]](http://home.cerge-ei.cz/gebicka/files/IV_Simultaneity.pdf)， [[PDF2]](https://webs.wofford.edu/pechwj/Using%20Electoral%20Cycles%20in%20Police%20Hiring%20to%20estimate%20the%20Effects%20of%20Police%20on%20Crime.pdf)
   - Levitt, Steven D. 2002. Using Electoral Cycles in Police Hiring to Estimate the Effect of Police on Crime: Reply. **American Economic Review**. 92:4, pp. 1244–50. [[PDF]](http://pricetheory.uchicago.edu/levitt/Papers/LevittUsingElectoralCycles2002.pdf)
   - Acemoglu, D., S. Johnson, and J. A. Robinson. 2001. The colonial origins of comparative development: An empirical investigation. **American Economic Review** 91: 1369–1401. [[PDF]](https://economics.mit.edu/files/4123), [[Data & Programs]](https://economics.mit.edu/faculty/acemoglu/data/ajr2001)
   - 王美今、林建浩、胡毅，IV估计框架下模型设定检验问题的讨论，**统计研究**, 2012年第2期。[[PDF-CNKI]](http://www.cnki.com.cn/Article/CJFDTotal-TJYJ201202013.htm), [[PDF-Download]](http://search.cnki.net/down/default.aspx?filename=TJYJ201202013&dbcode=CJFD&year=2012&dflag=pdfdown)

  
#### 专题 2：弱 IV 与近似外生 IV
- 弱 IV 下的稳健推断
- 近似外生 IV 下的稳健推断
- **应用场景：** 弱工具变量和工具变量非严格外生时还能用 IV 估计吗？
- **应对方法：** 采用再抽样（resampling，Berkowitz et al., 2012）或贝叶斯的方法校正近似外生性的影响（Conley et al., 2012；Kraay, 2012）。
- 论文解读：
  - Nunn, Nathan, and Leonard Wantchekon, 2011, The Slave Trade and the Origins of Mistrust in Africa. **American Economic Review** 101(7): 3221-52\. [[PDF](https://gitee.com/arlionn/stata_training/raw/master/PDF/Nunn_Wantchekon_2011_AER.pdf)]。&ensp; 该文展示了 **工具变量方法各种技巧**。主要讨论非洲奴隶贸易如何影响今天人际间的不信任，并用种族到海岸线的距离作为奴隶贸易强度的工具变量。文章从 OLS 估计出发，以评估其受选择性偏误的影响程度；继而使用工具变量法，通过“无第一阶段”**证伪检验** 和“工具变量疑似内生”证伪检验来论证 IVs 的合理性；最后展示了如何通过精妙的控制来讨论因果关系的作用渠道。
  - Clarke, D., & Matta, Benjamín. Practical considerations for questionable IVs. **Stata Journal**, 2018, 18(3), 663-691. [[PDF]](https://sci-hub.tw/10.1177/1536867X1801800308)
  - 林建浩、赵子乐，均衡发展的隐形壁垒：方言、制度与技术扩散，**经济研究**, 2017 年第 9 期。[[PDF]](http://www.cnki.com.cn/Article/CJFDTotal-JJYJ201709014.htm)

  
#### 专题 3：面板数据模型
- 面板数据的固定效应与随机效应建模
- 动态面板模型的内生性问题与 GMM 估计
- 论文解读：
  - Acemoglu, D., & Simon Johnson & James A. Robinson & Pierre Yared, 2008. "Income and Democracy," **American Economic Review**, 98(3): 808-842. [[PDF]](https://www8.gsb.columbia.edu/sites/financialstudies/files/files/democracy.pdf), [[PDF2]](https://economics.mit.edu/files/5677), [[Data & Programs]](https://economics.mit.edu/faculty/acemoglu/data/ajry2008)。该文涉及固定效应，动态面板 (FD-GMM, SYS-GMM) 以及各类稳健性检验。

#### 专题 4：倍分法 
- DID 基本应用：估计与检验
- DID 拓展应用：动态 DID 及三重差分 (DDD)
- 论文解读：
  - Beck, T., R. Levine, A. Levkov (2010) Beck, T., R. Levine, A. Levkov, 2010, Big bad banks? The winners and losers from bank deregulation in the united states, **Journal of Finance**, 65 (5): 1637-1667. [[PDF]](https://sci-hub.tw/10.1111/j.1540-6261.2010.01589.x)。这篇文章是应用多期 DID 的经典论文，探讨了美国各州陆续 「放松银行管制」后的产生的政策效果——收入不均。
&emsp;


---
### 王群勇 (5-8 讲)
   
   
#### 专题 5：匹配分析 (matching)   
- 最小近邻匹配简介
- 倾向得分匹配简介
- 匹配方法：半径匹配、最近匹配、核匹配等
- 假设检验：共同支撑与平衡性检验
- 论文讲读：
  - Qunyong Wang, Z. Li, and X. Feng. Does the happiness of contemporary women in China depend on their husbands’ achievements? **Journal of Family and Economics Issues** 2019. [[PDF]](https://link.springer.com/content/pdf/10.1007%2Fs10834-019-09638-y.pdf)
  - Dehejia, R.H., and S.Wahba. Causal effects in non-experimental studies: Evaluating the evaluation of training program. **Journal of the American Statistical Association**, 1999, 94 (448): 1053-1062. [[PDF]](https://dl1.cuni.cz/pluginfile.php/99036/mod_resource/content/0/Dehejia_Wahba_1999_.pdf)

#### 专题 6：断点回归分析 (RDD) 
- RDD 简介
- 明确断点回归
- 模型检验：平滑性检验、平衡性检验、安慰剂检验、模型错误设定检验
- 模糊断点回归
- 论文讲读: 
  - Lee, D.S. 2008, Randomized experiments from nonramdom selection in U.S. house election. **Journal of Econometrics**, 142(2): 675-697. [[PDF]](http://sci-hub.tw/10.1016/j.jeconom.2007.05.004)
  - Fernanda, Nannicini, Perotti and Tabellini. 2013, The poitical resource curse. **American Economic Review**, 103(5): 1759-1796. [[PDF]](http://www.rperotti.com/doc/PoliticalresourceCurseAER2013.pdf)


#### 专题 7：离散选择模型的内生性问题与估计 
- 控制函数法 (Control Function Method)
- Probit/Logit 模型的内生性问题
- Poisson/负二项回归模型的内生性问题
- 面板数据情形下的处理方法
- 论文解读：
  - Irina Murtazashvili, Jeffrey M. Wooldridge, A control function approach to estimating switching regression models with endogenous explanatory variables and endogenous switching,
**Journal of Econometrics**, 2016， 190(2): 252-266. [[PDF]](https://quqi.gblhgk.com/s/880197/pKRzYIjU6zcqguH8)
  - Kosuke Oya, Properties of estimators of count data model with endogenous switching, Mathematics and Computers in Simulation, 2005, 68(5–6): 536-544. [[PDF]](http://sci-hub.tw/10.1016/j.matcom.2005.02.011)
  - Joseph V. Terza, Estimating count data models with endogenous switching: Sample selection and endogenous treatment effects,
Journal of Econometrics, 1998, 84(1): 129-154. [[PDF]](http://sci-hub.tw/10.1016/S0304-4076(97)00082-1)

#### 专题 8：内生处理效应模型
- 处理稀释和处理混入问题
- 线性模型的内生处理效应      
- Probit/Logit 模型的内生处理效应       
- Poisson 回归模型的内生处理效应      
- 排序回归模型 (MLogit, MProbit) 的内生处理效应 
- 内生性与样本选择的关系 
- 论文解读：
  - Angrist, J.D. Instrumental variables methods in experimental criminological research: what, why and how. Journal of Experimental Criminology, 2006, 2(1):23-44. [[PDF]](http://masteringmetrics.com/wp-content/uploads/2015/02/Angrist_2006.pdf)
  - Na Wu, Qunyong Wang. Wage penalty of overeducation: New micro-evidence from China. China Economic Review, 2018(50): 206-217. [[PDF]](http://sci-hub.tw/10.1016/j.chieco.2018.04.006)


&emsp;

 
---
## 4. 报名信息

- **主办方：** 太原君泉教育咨询有限公司
- **标准费用**(含报名费、材料费)，差旅及食宿费自理：
  - 全价：4400元/人
  - 预报名价 (11 月 1 日前报名) 4200元/人
  - 团报价：3800元/人（三人及以上）
  - 学生价：3900元/人（报到时需提供学生证原件）
- **老学员优惠：**
  - 参加过一次主办方培训的老学员：3700元/人
  - 参加过两次及以上主办方培训的老学员：3500元/人
- **友情高校感恩优惠：**  
  我们诚挚地感谢曾经团报的高校，来自以下高校的老师和学生可享受感恩价 3600 元/人：(按首字母拼音排序) **安徽财经大学、东北林业大学、河北大学、河北经贸大学、河南财经政法大学、暨南大学、金陵科技学院、南开大学、清华大学、山西财经大学、山西大学、太原科技大学、西安建筑科技大学、西南财经大学、厦门大学、运城学院、中南财经政法大学、中南大学、中山大学、中央财经大学**。
- **Note：** 以上各项优惠不能叠加使用。
- **联系方式：**
  - 邮箱：[wjx004@sina.com](wjx004@sina.com)
  - 电话 (微信同号)：王老师 18903405450 ；李老师 ‭18636102467
  - 对公账户：
    - 户名：太原君泉教育咨询有限公司  
    - 账号：35117530000023891 (山西省太原市晋商银行南中环支行)

> **报名链接：** [http://junquan18903405450.mikecrm.com/ipdvr0X](http://junquan18903405450.mikecrm.com/ipdvr0X)         
或 长按/扫描二维码报名：   
![连享会-2019.11-内生性专题-报名二维码](https://images.gitee.com/uploads/images/2019/0908/160439_4c0a01e6_1522177.png "连享会-2019.11-内生性专题班-报名二维码.png")
>
>**温馨提示：** 按报名顺序挑选\安排座位


&emsp;

## 5. 诚聘助教

- [诚聘课程助教 (6名)](https://www.wjx.top/m/45161144.aspx)，截止时间：2019年9月27日
- 报名网址：https://www.wjx.top/m/45161144.aspx          
  扫码在线报名：
> ![连享会-2019.11-内生性专题班-助教招聘](https://images.gitee.com/uploads/images/2019/0905/155357_135abd3b_1522177.png)

&emsp;



## 6. 交通和住宿

- **校内宾馆：** 南京财经大学福州路校区培训中心 (鼓楼区铁路北街128号) ([百度地图](https://j.map.baidu.com/29/EaK)) 
- 住宿小知士：
南京财经大学福州路校区培训中心宾馆位于南京财经大学校内，上课、午休方便快捷，住宿环境可以参考以下图片：

[![连享会-2019.11-内生性专题-住宿和教室](https://images.gitee.com/uploads/images/2019/0905/155357_7a62ffe9_1522177.jpeg)](https://zhuanlan.zhihu.com/p/81310667)


&emsp;

----

> Stata连享会 [计量专题](https://mp.weixin.qq.com/s/hWtncj56PeFNL4yg2-va0Q)  || [公众号合集](https://mp.weixin.qq.com/s/RwkuPpLS7bI5C5OhRqjkOw)

[![点击查看完整推文列表](https://images.gitee.com/uploads/images/2019/0905/165511_e0768176_1522177.png)](https://mp.weixin.qq.com/s/RwkuPpLS7bI5C5OhRqjkOw)

---
![欢迎加入Stata连享会(公众号: StataChina)](https://images.gitee.com/uploads/images/2019/0905/165511_16a4f7b0_1522177.jpeg "扫码关注 Stata 连享会")
