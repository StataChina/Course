&emsp; 

> 连享会 · 公开课：<br>Stata 学习经验分享：从入门到进阶

&emsp;



![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/龙志能公开课V6.png)

&emsp;

---

**目录**

[[TOC]]

---

&emsp;

## 1. 课程概览

- **听课方式：** 网络直播。支持手机、iPad、电脑等。
- **直播嘉宾**：龙志能 (上海财经大学博士生)
- **费用/软件**：免费，Stata
- **时间**：2020 年 11 月 10 日，19:00-20:00   
- **课程主页：** <https://gitee.com/arlionn/StataBin> 
  - 回放：本课程提供长期回放，可前往主页查看回放信息。
  - 课程资料：课程中涉及的资料和链接会通过课程主页发布。 
- **课程咨询：** 李老师-18636102467（微信同号）
- **学习方式：** 扫描海报中的二维码，加入课程群，随后统一发布听课链接。

&emsp; 

## 2. 课程导言

面对海量 Stata 资料，你是否时常不知从何学起？我也是，初识 Stata 的那个夏天，一时间毫无头绪，几月有余，学会了些许命令，直到看过同门师兄 Stata 操作，恍然大悟。我的学习方式恍如无头苍蝇，四处乱窜，毫无成效。

厉害的师兄就在身边，向他学习事半功倍。在师兄的指导和有计划学习下，我总算成功入门。我看过很多资料，上过不少课程；在连享会做助教，在实践中不断学习，对于 Stata 使用有一定进步。本次分享希望将我的经验和教训分享给需要的朋友。

本次课程旨在帮助大家搭建 Stata 入门和进阶的基本框架，推荐精选权威的各种资源，以及分享高效解决学习中疑难的基本方法。主要内容包括：

### Stata 入门
- **少走点弯路：初学者的学习路线**。分别从计量基础和 Stata 操作层面总结入门 Stata 所需要学习和掌握的关键知识点。
- **武林秘籍：学习资源分享**。对应 Stata 入门需学习的重点，精准推荐系统学习的书籍手册、推文链接和学习视频。

### Stata 进阶
- **步步为营：进阶者的学习路线探讨**。我们该学习哪些主要的计量方法和模型？Stata 操作层面要如何提升自己？要学习编程吗？
- **进阶者的学习资源推荐**。对应 Stata 进阶需掌握的重点，精选推荐便于查阅的书籍手册、推文链接和视频课程。

### 扫除取经路上的妖魔鬼怪
- **如何解决学习中遇到的难题**。学习中总会遇到各种困难，我们该如何快速找到解决方法？去哪里求助？如何求助？

### 说在最后
- 收藏 != 学会 (贪心+自我安慰)
- 看过 != 学会 (囫囵吞枣+眼高手低)
- 实际操作 + 分享讲解 + 写篇论文 = 真会了


&emsp;

## 3. 课程提要

- **Stata 入门**：学习路线总结
  - 基础不牢，地动山摇；基础 = 知识架构 + 核心概念
  - 初级计量基础：多元回归；模型筛选；虚拟变量和交乘项
  - Stata 基础操作
    - 基本数据处理（导入、合并、清洗、缺失值、异常值、重复值）
    - 描述性统计与OLS回归
    - 固定效应模型回归
  - Stata 入门的学习资源分享
    - 书籍、手册、推文、视频
- **Stata 进阶**：「干中学」最有效！
  - 中高级计量基础【适用条件+正确理解】
    - 内生性问题理解
    - 匹配方法（matching）
    - 工具变量法（IV）
    - 双重差分方法（DID）
    - 断点回归（RD）
  - Stata 进阶学习
    - 高级数据处理（处理复杂数据，Python和Stata融合）
    - 构建复杂变量（盈余管理，股价崩盘，过度投资）
    - 正确使用命令+适用条件检验+安慰剂检验
  - Stata 进阶的学习资源推荐
- **Q：** 如何解决学习中遇到的难题？
  - 搜索引擎、微信搜索
  - 垂直网站
  - 常见问题解答（FAQs）
  - 微信讨论群
  - 如何提问？如何发邮件？



&emsp;

## 4. 课程特色

- 短小精悍：快速了解 Stata 入门和进阶的基本框架和解决难题的各种方案
- 讲义资料：分享电子版课件，分享的框架和资源可用于指导自身学习。
- 课后答疑：课程结束后，分享嘉宾将提供问题解答。



&emsp;

## 5. 嘉宾简介

**龙志能**，上海财经大学博士生，主要研究方向为公司财务与公司治理，在《外国经济与管理》、《Research in International Business and Finance》发表论文。现为连享会直播课助教，曾参与 Stata 暑期班助教工作。参与并负责 2020 年 Stata 暑期班助教工作受到老师和学员的一致好评。

<img style="width: 150px" src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/20201103-龙志能小图.jpg">



&emsp;

## 6. 相关资料

- **预习资料：** [连享会视频公开课 - Stata33 讲](https://lianxh.duanshu.com/#/brief/course/b22b17ee02c24015ae759478697df2a0)：
- **连享会推文：** [Stata入门](https://www.lianxh.cn/blogs/16.html)；[Stata教程](https://www.lianxh.cn/blogs/17.html)；[Stata资源](https://www.lianxh.cn/blogs/35.html)
- **进阶内容：** [Stata 寒假班](https://gitee.com/lianxh/PX) 对暂元、循环语句、数据合并等会有进一步的讲解 (即将上线)。

&emsp;

&emsp;

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Lianxh_装饰黄线.png)

&emsp;

&emsp;

## 相关课程

&emsp;

> **连享会-直播课** 上线了！  
>  <http://lianxh.duanshu.com>

> **免费公开课：**
>
> - [直击面板数据模型](https://lianxh.duanshu.com/#/brief/course/7d1d3266e07d424dbeb3926170835b38) - 连玉君，时长：1 小时 40 分钟
> - [Stata 33 讲](https://lianxh.duanshu.com/#/brief/course/b22b17ee02c24015ae759478697df2a0) - 连玉君, 每讲 15 分钟.
> - 部分直播课 [课程资料下载](https://gitee.com/arlionn/Live) (PPT，dofiles 等)

&emsp;

---

### 课程一览

> 支持回看，所有课程可以随时购买观看。

| 专题                                                                                                      | 嘉宾   | 直播/回看视频                                                                                                                                                                                                                                                                            |
| --------------------------------------------------------------------------------------------------------- | ------ | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| &#x2B50; **[最新专题](https://www.lianxh.cn/news/46917f1076104.html)** &#x2B50;                           |        | DSGE, 因果推断, 空间计量等                                                                                                                                                                                                                                                               |
| &#x2B55; **[Stata 数据清洗](https://lianxh.duanshu.com/#/brief/course/c5193f0e6e414a7e889a8ff9aeb4aaef)** | 游万海 | [直播, 2 小时](https://www.lianxh.cn/news/f785de82434c1.html)，已上线                                                                                                                                                                                                                    |
| 研究设计                                                                                                  | 连玉君 | [我的特斯拉-实证研究设计](https://lianxh.duanshu.com/#/course/5ae82756cc1b478c872a63cbca4f0a5e)，[-幻灯片-](https://gitee.com/arlionn/Live/tree/master/%E6%88%91%E7%9A%84%E7%89%B9%E6%96%AF%E6%8B%89-%E5%AE%9E%E8%AF%81%E7%A0%94%E7%A9%B6%E8%AE%BE%E8%AE%A1-%E8%BF%9E%E7%8E%89%E5%90%9B) |
| 面板模型                                                                                                  | 连玉君 | [动态面板模型](https://efves.duanshu.com/#/brief/course/3c3ac06108594577a6e3112323d93f3e)，[-幻灯片-](https://quqi.gblhgk.com/s/880197/o7tDK5tHd0YOlYJl)                                                                                                                                 |
| 面板模型                                                                                                  | 连玉君 | [直击面板数据模型](https://lianxh.duanshu.com/#/brief/course/7d1d3266e07d424dbeb3926170835b38) [免费公开课，2 小时]                                                                                                                                                                      |

> Note: 部分课程的资料，PPT 等可以前往 [连享会-直播课](https://gitee.com/arlionn/Live) 主页查看，下载。

&emsp;

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lianxhbottom01.png)

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lianxhbottom02.png)

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lianxhbottom03.png)

&emsp;

---

> #### 关于我们

- **Stata 连享会** 由中山大学连玉君老师团队创办，定期分享实证分析经验。[直播间](http://lianxh.duanshu.com) 有很多视频课程，可以随时观看。
- [连享会-主页](https://www.lianxh.cn) 和 [知乎专栏](https://www.zhihu.com/people/arlionn/)，300+ 推文，实证分析不再抓狂。


&emsp;

> 连享会小程序：扫一扫，看推文，看视频……

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会小程序二维码180.png)

&emsp;

> 扫码加入连享会微信群，提问交流更方便

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会-学习交流微信群001-150.jpg)

> &#x270F; 连享会学习群-常见问题解答汇总：  
> &#x2728; <https://gitee.com/arlionn/WD>



