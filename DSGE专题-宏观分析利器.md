&emsp;

# 连享会·计量专题：DSGE 模型及应用

&emsp; 

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会-DSGE-海报600.png)

&emsp; 

&emsp;


## 1. 课程概览

- **授课方式：** 网络直播 (7 天回放)，缴费后主办方发送邀请码点击邀请码即可进入在线课堂，收看直播无需安装任何软件。
- **时间：** 共 4 天。
  - 2020 年 9.19-20 (周六，周日)
  - 2020 年 9.26-27 (周六，周日) 
  - **Note：** 上午 9:00-12:00; 下午 2:30-5:30；课后 30-60 分钟答疑
- **授课嘉宾：** 朱传奇 (中山大学)
- **软件/课件：** Matlab/Dynare，提供全套 Malab/Dynare 实操程序和文件 (开课前一周发送)
- **课程主页：** <https://gitee.com/arlionn/DSGE>
- **报名链接：** <http://junquan18903405450.mikecrm.com/HSOXMPV>

&emsp; 


### 课程导引

经过三十多年的发展，动态随机一般均衡模型（Dynamic Stochastic General Equilibrium, 简称 DSGE）已成为当代宏观经济学研究和分析主流框架，被众多经济学家和政策制定者们用于经济分析和政策研究。

作为宏观经济研究的主流框架，伴随着以 Dynare 为代表的软件包不断的开发和优化，DSGE 模型不再是少数研究者的专属方法，而逐渐成为所有宏观经济学习者和专业研究人员的日常工具之一。

遗憾的是，对初学者而言，往往在花费了大量的时间和心血后，仍然没有信心和能力应用 DSGE 展开分析。这种挫败感令人颇为沮丧。这是因为，DSGE 模型的构建是一个系统性的过程，需要综合应用多种经济学理论、计量模型和数值方法。尤其是模型中涉及到的数值方法和计量方法往往有较高的技术门槛，让部分有志于宏观经济的研究者望而却步。

为此，本培训课程专门针对 DSGE 模型初学者和宏观经济研究者，系统全面地介绍 DSGE 模型的相关理论及计量方法。我们的目标是，让初学者能在完成本期课程后，能够掌握经典 DSGE 模型的核心思想和推导过程，能对经典文献进行模型复制和结果再现，并能较为快速地阅读 Top 期刊，理解其中的 DSGE 模型，为将来构建自己的 DSGE 模型和从事基于 DSGE 模型的政策研究提供帮助

为了让大家对 DSGE 有所了解，主讲嘉宾朱传奇老师精心录制了两个小视频，对 **什么是 DSGE？**，**如何学习 DSGE?** 等问题进行了解答，希望能有助于各位顺利开启 DSGE 的大门。 

&emsp;

> &#x1F3A6; **视频 1：** [什么是 DSGE ？](https://weibo.com/tv/show/1034:4541935017197620?from=old_pc_videoshow)

&emsp;

> **视频 2：** [如何学习 DSGE ?](https://weibo.com/tv/show/1034:4541772697894921?from=old_pc_videoshow)

&emsp;

--- - --

&emsp; 

### 授课嘉宾

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/朱传奇工作照180.jpg "朱传奇(中山大学)")

**[朱传奇](https://lingnan.sysu.edu.cn/faculty/zhuchuanqi)** ，波士顿学院经济学博士，中山大学助理教授，美国联邦储备银行波士顿分行访问学者。主要研究领域为宏观经济学和计量经济学，在 Economics Letters、《世界经济》等国内外学术刊物上发表多篇论文，主持多项国家和省级基金项目，曾应邀多次讲授「DSGE模型培训」短期前沿课程并广受好评。


&emsp; 



## 2. 课程内容

> #### 第 1 讲：课程简介和宏观经济数据处理 [3学时]

- 宏观经济学发展历史
- DSGE 模型介绍
- 宏观经济数据处理
  - 宏观经济数据收集
  - 趋势剔除和周期提取
    - H-P滤波
    - B-P滤波
  - 经济周期统计描述

> #### 第 2 讲：Matlab介绍和真实商业周期(RBC)模型-I [3学时] 

- MATLAB 介绍及上机练习（2 课时）
  - Matlab程序入门
  - 利用Matlab处理宏观经济数据
- 真实商业周模型-I (Real Business Cycles, RBC)
  - RBC 基准模型介绍
    - 模型构建
    - 均衡条件推导
  - 参考文献：King, R. G., C. I. Plosser, S. T. Rebelo, **1988**, Production, growth and business cycles, **Journal of Monetary Economics**, 21 (2-3): 195-232. [-PDF-](https://sci-hub.tw/10.1016/0304-3932(88)90030-x)

> #### 第 3 讲：真实商业周期模型-II: 求解和参数校准 [3 课时]

- DSGE 模型近似及求解
   - 对数线性化
   - 线性差分方程组求解
   - Blanchard-Kahn（[1980](http://dept.ku.edu/~empirics/Emp-Coffee/blanchard-kahn_eca80.pdf)）方法
   - 广义Schur方法 
- 参数校准

> #### 第 4 讲：真实商业周期模型-III: 模型拓展及程序实现 [3 课时]

- Money-in-Utility 模型
    - 模型构建及推导
    - 参数校准
- Cash-in-Advance 模型
    - 模型构建及推导
    - 参数校准
- Matlab上机练习
    - RBC模型程序编写
    - RBC模型模拟

> #### 第 5 讲：Dynare 安装及使用 [3 课时]

- Dyanre安装与配置
    - Dynare程序安装
    - Dynare程序配置
- Dynare基本应用：以 RBC 模型为例
    - Dynare 文件结构及编写
    - Dyanre 程序运行
    - Dynare 结果分析及调用


> #### 第 6 讲：新凯恩斯主义（New-Keynesian）模型-I: 模型介绍 [3 课时]

- New-Keynesian 模型简介
- 家庭、厂商部门及政府部门
- 货币规则（价格型规则和数量型规则）
- 粘性价格定价法则（Calvo定价）
- 均衡条件详细推导


> #### 第 7 讲：新凯恩斯主义模型-II: 求解、校准及脉冲响应分析 [3 课时]

- 均衡条件求解
- 对数线性化及线性求解
- 参数校准及模型模拟
- 脉冲响应结果分析
    
    
> #### 第 8 讲：中等规模DSGE模型: 经典论文解读 [3 课时]

- 经典论文解读：Smets, F., R. Wouters, **2007**, Shocks and frictions in us business cycles: A bayesian dsge approach, **American Economic Review**, 97 (3): 586-606. [-PDF-](https://sci-hub.tw/10.1257/AER.97.3.586)
   - 模型介绍
   - 均衡条件推导
   - Dynare程序论文复制
- DSGE模型的研究前沿
   - 金融摩擦
   - 非线性模型
   - DSGE模型的相关计量及计算方法


## 3. 参考资料

### 3.1 必读经典文献

> [在线浏览/下载](https://www.jianguoyun.com/p/DYK7Tb0QtKiFCBjVprUD )

1. Blanchard, O. J., C. M. Kahn, **1980**, The solution of linear difference models under rational expectations, **Econometrica**, 48 (5): 1305-1311. [-PDF-](http://dept.ku.edu/~empirics/Emp-Coffee/blanchard-kahn_eca80.pdf)
2. Chang, C., K. Chen, D. F. Waggoner, T. A. Zha, **2016**, Trends and cycles in china's macroeconomy, **NBER macroeconomics annual**, 30 (1): 1-84. [-PDF-](https://sci-hub.tw/10.2139/SSRN.2618708)
3. Christiano, L. J., M. Eichenbaum, C. L. Evans, **2005**, Nominal rigidities and the dynamic effects of a shock to monetary policy, **Journal of Political Economy**, 113 (1): 1-45. [-PDF-](https://sci-hub.tw/10.1086/426038)
4. King, R. G., C. I. Plosser, S. T. Rebelo, **1988**, Production, growth and business cycles, **Journal of Monetary Economics**, 21 (2-3): 195-232. [-PDF-](https://sci-hub.tw/10.1016/0304-3932(88)90030-x)
5. King, R. G., M. W. Watson, **1998**, The solution of singular linear difference systems under rational expectations, **International Economic Review**, 39 (4): 1015-1026. [-PDF-](https://sci-hub.tw/10.2307/2527350)
6. Smets, F., R. Wouters, **2007**, Shocks and frictions in us business cycles: A bayesian dsge approach, **American Economic Review**, 97 (3): 586-606. [-PDF-](https://sci-hub.tw/10.1257/AER.97.3.586)
7. Stock, J. H., M. W. Watson, **1998**, Business cycle fluctuations in u.S. Macroeconomic time series, **NBER Working Paper**. [-PDF-](https://sci-hub.tw/10.3386/W6528)

### 3.2 参考书目

1.  课程培训讲义、相关文献及参考程序(Matlab 和 Dynare Codes)
2.  David Dejong and Chetan Dave(2011), Structural Macroeconometrics, Princeton University Press. [-Link1-](https://bbs.pinggu.org/thread-1008216-1-1.html)，[-Link2-](https://1084.cnc.jg.com.cn/forum/201101/08/ac4ba9eba313/0.pdf)
3.  Fabio Canova(2008), Methods for Applied Macroeconomic Research, Princeton University Press. [-Link-](https://bbs.pinggu.org/thread-1026354-1-1.html)
4.  David Houceque, (2005), Introduction to Matlab for Engineering Students, [-PDF1-](https://www.mccormick.northwestern.edu/documents/students/undergraduate/introduction-to-matlab.pdf)，[-PDF2-](http://read.pudn.com/downloads648/ebook/2625889/matlab.pdf)
5.  刘斌（2016），动态随机一般均衡模型及其应用，中国金融出版社
6.  李向阳（2018），动态随机一般均衡（DSGE）模型：理论、方法和 Dynare 实践，清华大学出版社
7.  朱军（2019），高级财政学 II-DSGE 的视角及应用前沿：模型分解与编程，上海财经大学出版社

&emsp; 

## 4. 报名和缴费信息

- **主办方：** 太原君泉教育咨询有限公司
- **标准费用** 4800 元/人 (全价)
- **优惠方案**：
  - **直播课老学员：** 4500元/人  
  - **专题课老学员：** 4000元/人  
- **温馨提示：** 以上各项优惠不能叠加使用。
- **联系方式：**
  - 邮箱：[wjx004@sina.com](wjx004@sina.com)
  - 王老师：18903405450 (微信同号)
  - 李老师：‭18636102467 (微信同号)


### 报名链接

> **报名链接：** <http://junquan18903405450.mikecrm.com/HSOXMPV>                

> &#x1F449;  长按/扫描二维码报名： 

<img style="width: 150px" src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/报名：DSGE模型基础及政策应用.png">


### 缴费方式

> **方式1：对公转账**

- 户名：太原君泉教育咨询有限公司  
- 账号：35117530000023891 (山西省太原市晋商银行南中环支行)
- **温馨提示：** 对公转账时，请务必提供「**汇款人姓名-单位**」信息，以便确认。

> **方式2：扫码支付**

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/010705_0e5b373a_1522177.png)


> **温馨提示：** 扫码支付后，请将「**付款记录**」截屏发给王老师-18903405450（微信同号）


&emsp;


> **课程主页：** <https://gitee.com/arlionn/DSGE>  


&emsp; 

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/横条-远山03-窄版.jpg)

&emsp;

